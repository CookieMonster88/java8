package Optionals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Beata on 2017-02-09.
 */
public class OptionalTest {
    public static void main(String[] args) {

        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("Mickiewicza 1"));
        Person person = new Person("Jan", 23, addresses);

        String name = Optional.of(person)
                .filter(p -> p.getAge() > 23)
                .map(Person::getName)
                .orElse("BRAK");


        System.out.println(name);

//        Optional.of(null);

        if(Optional.ofNullable(null).isPresent()) {
            System.out.println("Istnieje");
        }

//        Optional.ofNullable(person)
//                .flatMap(person1 -> Optional.ofNullable(person1.getAddresses()))
//                .filter(a -> a.get)
    }
}

}
