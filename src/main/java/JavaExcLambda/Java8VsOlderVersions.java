package JavaExcLambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Beata on 2017-02-09.
 */
public class Java8VsOlderVersions {
    public static void main(String[] args) {
        oldSort();
        System.out.println("#### NEW SORT ###");
        newSort();
    }

    private static void oldSort() {
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println("Przed sortowaniem");
        System.out.println(names);
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return b.compareTo(a);
            }
        });

        System.out.println("Po sortowaniu");
        System.out.println(names);
    }


    private static void newSort() {
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println("Przed sortowaniem");
        System.out.println(names);
        Collections.sort(names, (a, b) -> b.compareTo(a));
        System.out.println("Po sortowaniu");
        System.out.println(names);
    }
}

}
