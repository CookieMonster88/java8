package Interfaces;

/**
 * Created by Beata on 2017-02-09.
 */
    public class MyClass implements FooInterface, FooInterfaceB {

        public static void main(String[] args) {
            new MyClass().foo();
        }

        @Override
        public void foo() {

        }
}
