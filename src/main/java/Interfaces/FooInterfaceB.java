package Interfaces;

/**
 * Created by Beata on 2017-02-09.
 */
public interface FooInterfaceB {
    default void foo() {
        System.out.println("Foo InterfaceB");
    }
}
