package Interfaces;

/**
 * Created by Beata on 2017-02-09.
 */
public interface MyData {
    default void print(String str) {
        if (!isNull(str))
            System.out.println("MyData : " + str);
    }

    static boolean isNull(String str) {
//    default boolean isNull(String str) {
        System.out.println("Interface Null Check");
        return str == null ? true : "".equals(str) ? true : false;
    }
}
